#CMPM170 Round 1 README

##Programmers

| FirstName | LastName | Email                                         | DiscordTag        |
|-----------|----------|-----------------------------------------------|-------------------|
| Alexander | Smirnov  | [asmirno1@ucsc.edu](mailto:asmirno1@ucsc.edu) | Alex Smirnov#3493 |
| Victoria  | Lima     | [vmlima@ucsc.edu](mailto:vmlima@ucsc.edu)     | Silverbird#4493   |
| Nicolas   | Ming     | [ncming@ucsc.edu](mailto:ncming@ucsc.edu)     | MingUhLing#2982   |

---

##Setup

For cloning and pushing to one of the repositories, you will need to either:

1. Reset the BitBucket password (assuming you have logged in with Google)
2. Create an ssh key for your account login as described [here](https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html).

To simplify git operations, check out the **Using SourceTree** section at the bottom of the page.

##Programming Conventions

####Spacing

- For most languages, let's keep it to 4 spaces per indent. If your text editor uses tabs, look up how to convert tabs into spaces.
- For web languages (js, html, css), if we need to write in them, let's use 2 spaces per indent.

####Style
- [C# Unity Community Style Guide](http://wiki.unity3d.com/index.php/Csharp_Coding_Guidelines#Style_Guidelines)

##Misc.

###Using SourceTree

Use these steps to clone from SourceTree. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You�ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you�d like to and then click **Clone**.
4. Open the directory you just created to see your repository�s files.

You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
