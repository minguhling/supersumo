using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class Pause : MonoBehaviour
{
    public bool IsGamePaused;
    GameObject[] pauseObjects;


    void Start()
    {
        //Find all UI elements at start of scene and hide them
        pauseObjects = GameObject.FindGameObjectsWithTag("pause");
        HideMenu();
    }

    void Update()
    {
        //Button bindings for opening pause menu
        if (Input.GetKey(KeyCode.Escape))
        {
            PauseGame();
        }
        if (Input.GetButton("Start1") || Input.GetButton("Start2"))
        {
            PauseGame();
        }
    }

    //Function to hide all menu objects
    void HideMenu()
    {
        foreach (GameObject obj in pauseObjects)
        {
            obj.SetActive(false);
        }
    }

    //Function to display all menu objects
    void ShowMenu()
    {
        foreach (GameObject obj in pauseObjects)
        {
            obj.SetActive(true);
        }
    }

    //Function to resume play from pause menu
    public void StartGame()
    {
        HideMenu();
        IsGamePaused = false;
        Time.timeScale = 1;
    }
    //Function to return to the main menu from pause menu
    public void MenuReturn()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("StartMenu");
    }

    //Function to pause the game and open pause menu
    void PauseGame()
    {
        ShowMenu();
        IsGamePaused = true;
        Time.timeScale = 0;
    }
}