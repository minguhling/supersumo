﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// super class for all animal abilities
public class Animal : MonoBehaviour
{
    // just override everything
    public virtual void PressA()
    {
        
    }
    public virtual void PressX()
    {

    }
    public virtual void PressB()
    {

    }
}
