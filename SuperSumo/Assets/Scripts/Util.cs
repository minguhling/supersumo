﻿using System;
using System.Collections.Generic;

public static class Util
{
    public static T[] YatesShuffle<T>(T[] itemsToShuffle)
    {
        // Fisher-Yates Random Shuffle, Inside-out variant
        // Returns a new IEnumerable with elements T shuffled, not modifying original

        T[] retItems = new T[itemsToShuffle.Length];

        Random rng = new Random();

        for (int i = 0; i < itemsToShuffle.Length; i++){
            int j = rng.Next(0, i + 1);
            if(!EqualityComparer<T>.Default.Equals(default(T), retItems[j])){
                retItems[i]=retItems[j];
            }
            retItems[j] = itemsToShuffle[i];
        }

        return retItems;
    }
}
