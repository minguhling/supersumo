﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    // Update is called once per frame

    public static int counter = 0;
    public static bool player1Alive = true;
    public static bool player2Alive = true;
    public static bool player3Alive = true;
    public static bool player4Alive = true;
    void Update()
    {

    }

    //Function to enter main game
    public void Click()
    {
        //Reset time in case called from pause menu
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");
    }

    //Function to quit the game
    public void QuitGame()
    {
        Application.Quit();
    }
}