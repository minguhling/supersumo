﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // the index for which player this is
    public int playerIndex;

    // private Rigidbody rb;
    public float moveSpeed = 6f;
    [Range(0, 1)]
    public float slerptime = 0.25f;

    private ParticleSystem ps;
    private string[] connectedControllers;
    private bool disconnected;

    void Start()
    {
        ps = transform.GetChild(0).GetComponent<ParticleSystem>();
        //CheckConnectivity();

    }

    void Update()
    {
        // turn on particles if you moving
        // only works for controllers right now
        if (Input.GetAxisRaw("LeftAnalogX" + playerIndex.ToString()) != 0 || Input.GetAxisRaw("LeftAnalogY" + playerIndex.ToString()) != 0)
        {
            if (!ps.isPlaying) ps.Play();
        }
        else
        {
            if (ps.isPlaying) ps.Stop();
        }


        // you need to check what player it is only for keyboard controls
        if (this.transform.tag + playerIndex.ToString() == "Player1")
        {

            // Movement for player 1 with keyboard controls
            if (Input.GetKey(KeyCode.A))
            {
                transform.Translate(Time.deltaTime * -10.0f, 0, 0, Space.World);
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 270, 0), slerptime);
            }
            if (Input.GetKey(KeyCode.D))
            {
                transform.Translate(Time.deltaTime * 10.0f, 0, 0, Space.World);
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 90, 0), slerptime);
            }
            if (Input.GetKey(KeyCode.W))
            {
                transform.Translate(0, 0, Time.deltaTime * 10.0f, Space.World);
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 360, 0), slerptime);
            }
            if (Input.GetKey(KeyCode.S))
            {
                transform.Translate(0, 0, Time.deltaTime * -10.0f, Space.World);
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 180, 0), slerptime);
            }


            // Force mechanics
            if (Input.GetKey(KeyCode.Q))
            {
                GetComponents<Animal>()[0].PressX();
            }
            else if (Input.GetKey(KeyCode.E))
            {
                GetComponents<Animal>()[0].PressA();
            }

        }
        else
        {//if (this.gameObject.tag == "player2") {

            // Movement for player 2 with keyboard controls
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                transform.Translate(Time.deltaTime * -10.0f, 0, 0, Space.World);
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 270, 0), slerptime);
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                transform.Translate(Time.deltaTime * 10.0f, 0, 0, Space.World);
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 90, 0), slerptime);
            }
            if (Input.GetKey(KeyCode.UpArrow))
            {
                transform.Translate(0, 0, Time.deltaTime * 10.0f, Space.World);
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 360, 0), slerptime);
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                transform.Translate(0, 0, Time.deltaTime * -10.0f, Space.World);
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 180, 0), slerptime);
            }

            // Force mechanics
            if (Input.GetKey(KeyCode.Comma))
            {
                GetComponents<Animal>()[0].PressX();
            }
            else if (Input.GetKey(KeyCode.Period))
            {
                GetComponents<Animal>()[0].PressA();
            }
        }

        // for xbox controls you can use the index to be more general
        // XBOX MOVEMENT
        transform.position += (Vector3.right * Input.GetAxis("LeftAnalogX" + playerIndex.ToString()) + Vector3.forward * -Input.GetAxis("LeftAnalogY" + playerIndex.ToString())) * moveSpeed * Time.deltaTime;
        if (Vector3.right * Input.GetAxis("LeftAnalogX" + playerIndex.ToString()) + Vector3.forward * -Input.GetAxis("LeftAnalogY" + playerIndex.ToString()) != Vector3.zero)
        {
            transform.rotation = Quaternion.LookRotation(Vector3.right * Input.GetAxis("LeftAnalogX" + playerIndex.ToString()) + Vector3.forward * -Input.GetAxis("LeftAnalogY" + playerIndex.ToString()));
        }

        // using literal magic to allow me to grab baseclasses by searching for the superclass
        if (Input.GetButtonDown("Left" + playerIndex.ToString()))
        {
            GetComponents<Animal>()[0].PressX();
        }
        else if (Input.GetButtonDown("Bottom" + playerIndex.ToString()))
        {
            GetComponents<Animal>()[0].PressA();
        }
        else if (Input.GetButtonDown("Right" + playerIndex.ToString()))
        {
            GetComponents<Animal>()[0].PressB();
        }
    }
    void CheckConnectivity()
    {
        //Checking to ensure the number of controllers matches the number of players
        int numPlayers = GameObject.Find("GameController").GetComponent<GameController>().numberOfPlayers;
        print(numPlayers);
        connectedControllers = Input.GetJoystickNames();

        if (numPlayers != connectedControllers.Length)
        {
            print("Disconnected Controller!");
            disconnected = true;
        }
    }
}