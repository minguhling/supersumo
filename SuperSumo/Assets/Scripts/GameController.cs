﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// this controller runs every time that level loads
public class GameController : MonoBehaviour {
    // static reference to this instance of gamecontroller
    public static GameController Instance { get; protected set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    // public variables
    public int        numberOfPlayers;
    //Setting numberOfPlayers to reflect choice at JoinGame scene
    public GameObject player;
    public Material[] doggoMaterials;
    public GameObject winningSpotLight;
    public Vector3    endCameraTarget;

    [Header("SPAWN MANAGER")]
    public Vector3[]  spawnLocations;

    // list of player gameobjects
    public List<GameObject> players = new List<GameObject>();
    int sizeofplayerslist;
    // make a round controller
    RoundController roundInfo;

    // keeps track of text on scoreboard
    public Text countText;
    public Text countText2;
    public Text countText3;
    public Text countText4;


    void Start () {
        // to play the game with controllers from the controller connect scene
        //numberOfPlayers = JoinGame.numPlayers;
        roundInfo = new RoundController(3);
        SpawnPlayers();
        setCountText();
        setCountText2();
        setCountText3();
        setCountText4();
    }

    // update
    void Update()
    {
        sizeofplayerslist = players.Count;
        // if there is one player left, increment the round counter for the player who won
        // then kill the player and respawn everyone
        if (sizeofplayerslist == 1)
        {
            int playernum = players[0].GetComponent<PlayerController>().playerIndex;
            switch (playernum)
            {
                case 1:
                    roundInfo.Player1RoundCount++;
                    setCountText();
                    break;
                case 2:
                    roundInfo.Player2RoundCount++;
                    setCountText2();
                    break;
                case 3:
                    roundInfo.Player3RoundCount++;
                    setCountText3();
                    break;
                default:
                    roundInfo.Player4RoundCount++;
                    setCountText4();
                    break;
            }
            EndState();
        }
    }


    //these are called to update the score on the scoreboard
    void setCountText()
    {
        countText.text = roundInfo.Player1RoundCount.ToString();
    }

    void setCountText2()
    {
        countText2.text = roundInfo.Player2RoundCount.ToString();
    }

    void setCountText3()
    {
        countText3.text = roundInfo.Player3RoundCount.ToString();
    }

    void setCountText4()
    {
        countText4.text = roundInfo.Player4RoundCount.ToString();
    }

    // spawns players at the desired locations
    // sets the player index for the player input
    // adds them to the players list
    private void SpawnPlayers()
    {
        for (int i = 0; i < numberOfPlayers; i++)
        {
            GameObject p = SpawnObjectAtPosition(player, spawnLocations[i]);
            p.GetComponent<PlayerController>().playerIndex = i + 1;
            p.GetComponent<Renderer>().material = doggoMaterials[i];
            players.Add(p);
        }
    }

    // a function that spawns an object at a position and returns a reference
    // this function can be called using GameController.Instance.SpawnObject...
    public GameObject SpawnObjectAtPosition(GameObject obj, Vector3 pos)
    {
        GameObject newObj = Instantiate(obj);
        newObj.transform.position = pos;
        return newObj;
    }

    // will control the end state when someone wins
    // zoom in on winner
    // wait a second
    // then reload the map
    public void EndState()
    {
        StartCoroutine(End(players[0]));
        players.Remove(players[0]);
    }

    // after 3 seconds it resets the game
    private float endTime = 3;
    private float lerpSpeed = 3;
    private IEnumerator End(GameObject winner)
    {
        float timer = 0;
        Camera main = Camera.main;
        float defaultSize = main.orthographicSize;
        Vector3 originalPosition = main.transform.position;

        // parent a spotlight to the winning animal 
        GameObject spotlight = SpawnObjectAtPosition(winningSpotLight, Vector3.zero);
        spotlight.transform.position = winner.transform.position + new Vector3(0, 10, 0);
        spotlight.transform.SetParent(winner.transform);

        while (timer <= endTime)
        {
            timer += Time.deltaTime;
            main.orthographicSize = Mathf.Lerp(main.orthographicSize, 10, Time.deltaTime * lerpSpeed);
            main.transform.position = Vector3.Lerp(main.transform.position, endCameraTarget, Time.deltaTime * lerpSpeed);
            yield return null;
        }

        // after the 3 seconds it loads the gameover scene
        // if any of the players counts is at or exceeds the roundWinValue then load the Gameover Scene
        if (roundInfo.RoundOver())
        {
            SceneManager.LoadScene("Gameover");
            yield return null;
        }else
        {
            // reset the cameras size to default
            main.orthographicSize = defaultSize;

            // after the wait
            Destroy(winner);
            SpawnPlayers();
            StartCoroutine(ScoreKeeper.Instance.MyDelay());
        }
    }

    public void killObject(GameObject obj)
    {
        // kill this object
        // remove the player
        players.Remove(obj);
        Destroy(obj);
    }

}