﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// gets monobehavior from animal supertype
public class Dog : Animal
{
    // can be changed later
    private float dashValue = .1f;
    private float dashSpeed = 70; // was 100

    private float forceValue = 80;
    private float raycastDistance = 10;
    private int numberOfRaycasts = 10;
    private float rayTime = .1f;

    private float aoeRange = 6;
    private float aoeForce = 10;

    private float knockUpTime = .5f;
    private float knockUpSpeed = 20;

    private float dashCoolDown = 1;
    private float barkCoolDown = .5f;
    bool canDash = true;
    bool canBark = true;

    // dash!
    public override void PressA()
    {
        base.PressA();
        // do dog A ability
        Debug.Log("Dog A ability");
        if (canDash)
        {
            canDash = false;
            StartCoroutine(CoolDash(dashCoolDown));
            StartCoroutine(LinearDash(dashValue));
        }
    }
    private IEnumerator CoolDash(float time)
    {
        float timer = 0;
        while (timer <= time)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        canDash = true;
    }
    private IEnumerator CoolBark(float time)
    {
        float timer = 0;
        while (timer <= time)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        canBark = true;
    }

    // bark!
    public override void PressX()
    {
        base.PressX();
        if (!canBark) return;
        canBark = false;
        StartCoroutine(CoolBark(barkCoolDown));
        GetComponent<AudioSource>().pitch = Random.Range(.8f, 1.2f);
        GetComponent<AudioSource>().Play();
        Debug.Log("Dog X ability");
        // raycast in a cone and push whatever it touches away with force

        // make a list of players in the range
        List<GameObject> hitPlayers = new List<GameObject>();

        // to start just one raycast
        RaycastHit hit;
        for (int i = 0; i < numberOfRaycasts; ++i)
        {
            Vector3 direction = transform.forward;
            direction = Quaternion.AngleAxis(-(45) + ((90 / numberOfRaycasts) * i), Vector3.up) * direction;
            Debug.DrawRay(transform.position, direction * raycastDistance, Color.red, rayTime);

            // raycast in a cone
            if (Physics.Raycast(transform.position, direction, out hit, raycastDistance))
            {
                if (hit.transform.tag == "Player" && hit.transform.gameObject != this.gameObject)
                {
                    if (hitPlayers.Contains(hit.transform.gameObject)) continue;
                    hitPlayers.Add(hit.transform.gameObject);
                }
            }
        }

        // hit everything in range
        foreach (GameObject obj in hitPlayers)
        {
            // add force in the direction
            float distance = Vector3.Distance(transform.position, obj.transform.position);

            // direction for force to go B - A
            Vector3 direction = obj.transform.position - transform.position;
            direction.Normalize();

            obj.transform.GetComponent<Rigidbody>().AddForce(direction * (forceValue / distance),
                                                             ForceMode.Impulse);
        }
    }

    // simple aoe push
    public override void PressB()
    {
        // simple aoe push
        foreach (GameObject obj in GameController.Instance.players)
        {
            if (this.gameObject == obj) continue;

            // check how far the other players are
            float distance = Vector3.Distance(transform.position, obj.transform.position);
            if (distance <= aoeRange)
            {

                // direction for force to go B - A
                Vector3 direction = obj.transform.position - transform.position;
                direction.Normalize();

                obj.transform.GetComponent<Rigidbody>().AddForce(direction * (aoeForce), ForceMode.Impulse);

                //StartCoroutine(KnockUp(obj));
            }
        }
    }


    // dashes for dashTime seconds at dashvalue speed
    // starts and ends at the same speed
    private IEnumerator LinearDash(float dashTime)
    {
        // disable the ability to move while dashing
        GetComponent<PlayerController>().enabled = false;
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<Rigidbody>().velocity = Vector3.zero;

        float timer = 0;
        while (timer < dashTime)
        {
            timer += Time.deltaTime;
            transform.position += transform.forward * Time.deltaTime * dashSpeed;
            yield return null;
        }

        // re enable the ability to move
        GetComponent<Rigidbody>().isKinematic = false;
        GetComponent<PlayerController>().enabled = true;
    }

    // knock people up
    private IEnumerator KnockUp(GameObject obj)
    {
        // disable the ability to move while dashing
        obj.GetComponent<PlayerController>().enabled = false;
        obj.GetComponent<Rigidbody>().useGravity = false;

        // store old position
        Vector3 originalPosition = obj.transform.position;

        float timer = 0;
        while (timer < knockUpTime / 2)
        {
            timer += Time.deltaTime;
            obj.transform.position += Vector3.up * Time.deltaTime * knockUpSpeed;
            yield return null;
        }
        while (timer > knockUpTime / 2 && timer < knockUpTime)
        {
            timer += Time.deltaTime;
            obj.transform.position -= Vector3.up * Time.deltaTime * knockUpSpeed;
            yield return null;
        }

        // re enable the ability to move
        obj.transform.position = originalPosition;
        obj.GetComponent<Rigidbody>().useGravity = true;
        obj.GetComponent<PlayerController>().enabled = true;
    }
}

