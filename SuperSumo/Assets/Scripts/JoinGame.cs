﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class JoinGame : MonoBehaviour
{
    public static int numPlayers;
    private int playersReady;
    private string[] connectedControllers;
    private bool[] playing = { false, false, false, false };
    private Dictionary<string, bool> ready = new Dictionary<string, bool>();

    public Button[] button;

    // Start is called before the first frame update
    void Start()
    {
        //int numPlayers = GameObject.Find("GameController").GetComponent<GameController>().numberOfPlayers;
        //print(numPlayers);
        connectedControllers = Input.GetJoystickNames();
        seeControllers(connectedControllers);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Start1")) {
            if (playing[0] == false)
                AddPlayer(0, playing, ready);

            else if (ready["player1"] == false) 
                StartCoroutine( ReadyToPlay(ready, 0));
        }
        else if(Input.GetButtonDown("Start2") && playing[1] == false) {
            if (playing[1] == false)
                AddPlayer(1, playing, ready);

            else if (ready["player2"] == false)
                StartCoroutine(ReadyToPlay(ready, 1));
        }
        else if(Input.GetButtonDown("Start3") && playing[2] == false) {
            if (playing[2] == false)
                AddPlayer(2, playing, ready);

            else if (ready["player3"] == false)
                StartCoroutine(ReadyToPlay(ready, 2));
        }
        else if(Input.GetButtonDown("Start4") && playing[3] == false) {
            if (playing[3] == false)
                AddPlayer(3, playing, ready);

            else if (ready["player4"] == false)
                StartCoroutine(ReadyToPlay(ready, 3));
        }
    }

    //Function to display the list of connected controllers to the console
    private void seeControllers(string[] controllers) {
        //Let's see the connected controllers in the console log
        print(controllers.Length + " connected controllers. Here are their names:");
        foreach(string joy in controllers) {
            print(joy);
        }
    }

    //Function to update the GUI and add a player to the game
    private void AddPlayer(int i, bool[] bools, Dictionary<string, bool> pairs) {
        //Activate Button
        button[i].Select();
        button[i].GetComponentInChildren<Text>().text = "Joined!";
        //Increase player count
        numPlayers += 1;
        bools[i] = true;
        pairs.Add("player" + (i+1).ToString(), false);
    }

    //Function to check if all players are ready to start the game
    private void AllPlayersReady()
    {
        if (ready.ContainsValue(false))
            return;
        else
            StartCoroutine(ChillOut(5));
    }
    //Function to flag a player as ready to start the game
    IEnumerator ReadyToPlay(Dictionary<string, bool> pairs, int i) {
        print("Starting: ReadyToPlay");
        button[i].GetComponentInChildren<Text>().text = "Ready to Play!";
        pairs["player" + (i + 1).ToString()] = true;
        //Wait to see if any other players join in the interim
        yield return new WaitForSeconds(3);
        //Then check to see if all players are ready
        AllPlayersReady();
    }

    //Function to start a timer, then load the level
    IEnumerator ChillOut(int time)
    {
        GameObject.Find("TitleText").GetComponent<Text>().text = "Let's Get Ready to Play!";
        yield return new WaitForSeconds(time);
        SceneManager.LoadScene("LavaLevel");
    }
}
