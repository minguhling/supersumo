﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Death : MonoBehaviour {
    public float yDeath = -5;
	// Update is called once per frame
	void Update () {
        if(transform.position.y <= yDeath)
        {
            GameController.Instance.killObject(this.gameObject);
        }
    }
}
