﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gameover : MonoBehaviour
{
    bool w1 = false;
    bool w2 = false;
    bool w3 = false;
    bool w4 = false;
    // Update is called once per frame
    void Update()
    {
        if (Menu.counter == 1)
        {
            if (Menu.player1Alive == true)
            {
                w1 = true;
            }
            else if (Menu.player2Alive == true)
            {
                w2 = true;
            }
            else if (Menu.player3Alive == true)
            {

            }
            else if (Menu.player4Alive == true)
            {

            }
        }

    }

    public void Click()
    {
        SceneManager.LoadScene("MainMenu");
        Menu.counter = 0;
        Menu.player1Alive = true;
        Menu.player2Alive = true;
        Menu.player3Alive = true;
        Menu.player4Alive = true;
    }

    public void Retry()
    {
        SceneManager.LoadScene("LavaLevel");
        Menu.counter = 0;
        Menu.player1Alive = true;
        Menu.player2Alive = true;
        Menu.player3Alive = true;
        Menu.player4Alive = true;
    }

    private void OnGUI()
    {
        if (w1==true)
        {
            GUI.Label(new Rect(Screen.width * 0.5f, Screen.height * 0.5f, 100, 30), "???????????");
        }
        else if (w2 == true)
        {
            GUI.Label(new Rect(Screen.width * 0.5f, Screen.height * 0.5f, 100, 30), "!!!!!!!!!!");
        }
        else if (w3 == true)
        {

        }
        else if (w4 == true)
        {

        }
    }
}
