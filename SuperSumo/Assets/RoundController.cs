﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// keeps track of round information
// im just moving the code so we have more space in files
public class RoundController
{
    // keeps track of the amount of rounds needed to win and the rounds that the player won
    public int RoundWinValue { get; private set; }
    public int Player1RoundCount { get; set; }
    public int Player2RoundCount { get; set; }
    public int Player3RoundCount { get; set; }
    public int Player4RoundCount { get; set; }

    // returns a bool if the round it over
    public bool RoundOver()
    {
        if(Player1RoundCount >= RoundWinValue || Player2RoundCount >= RoundWinValue || Player3RoundCount >= RoundWinValue || Player4RoundCount >= RoundWinValue)
        {
            return true;
        }else
        {
            return false;
        }
    }

    // constructor takes in how many matches in a round
    public RoundController(int winValue)
    {
        RoundWinValue = winValue;
    }
}
