﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScoreKeeper : MonoBehaviour
{
    public static ScoreKeeper Instance { get; protected set; }

    GameObject[] scoreObjects;
    RoundController roundInfo;

    private float sec = 3f;

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        scoreObjects = GameObject.FindGameObjectsWithTag("scoreupdate");
        StartCoroutine(MyDelay());
    }

    // Update is called once per frame
    void Update()
    {
        if (!roundInfo.RoundOver())
        {
            StartCoroutine(MyDelay());
        }
    }

    void HideMenu()
    {
        Debug.Log("in Hidemenu");
        foreach (GameObject obj in scoreObjects)
        {
            obj.SetActive(false);
        }
    }

    void ShowMenu()
    {
        Debug.Log("in showmenu");
        foreach (GameObject obj in scoreObjects)
        {
            obj.SetActive(true);
        }
    }

    public IEnumerator MyDelay()
    {
            Debug.Log("in delay");
            ShowMenu();
            yield return new WaitForSeconds(sec);
            HideMenu();
    }
}
